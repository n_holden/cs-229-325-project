import numpy as np
from qfunction import QFunction
from param_reader import readParams

def Agent(lightProgram, params):
    agent, = readParams(params, 'AgentType', [('agent', 'Q')])
    print("agent: %s" % agent)
    if agent == 'LQF':
        return LQFAgent(lightProgram, params)
    elif agent == 'Q':
        return QAgent(lightProgram, params)
    elif agent == 'cycle':
        return CycleAgent(lightProgram, params)
    elif agent == 'random':
        return AgentBase(lightProgram, params)
    else:
        raise Exception('Unknown agent type: %s' % agent)

class AgentBase(object):
    """Models a single traffic light. By default, chooses actions randomly.

    Attributes
    ----------
    lightProgram: LightProgram
    numActions: dict {id: int}
    """
    def __init__(self, lightProgram, params):
        self.lightProgram = lightProgram
        self.numActions = lightProgram.numActions

    def chooseAction(self, features, train=True):
        """Picks an action based on a the features. Default implementation
        chooses actions randomly. Can be overriden by subclass.
        """
        return np.random.choice(self.numActions)

    def addTrainingExample(self, features, action, reward, newFeatures):
        pass

    def getModel(self):
        return None

    def setModel(self, model):
        pass

class LQFAgent(AgentBase):
    """Models a single traffic light. Chooses actions using LQF algorithm.

    Attributes
    ----------
    lightProgram: LightProgram
    numActions: dict {id: int}
    onlyStopped: boolean, calculate queue lengths based on stopped or all vehicles
    """
    def __init__(self, lightProgram, params):
        super(LQFAgent, self).__init__(lightProgram, params)
        self.onlyStopped, = readParams(params, 'LQFAgent', [
            ('onlyStopped', False)
        ])

    def chooseAction(self, features, train=True):
        """Picks the action to let the phase for the longest queue be green."""
        queueLengths = [
            phase.queueLength(self.onlyStopped)
            for phase in self.lightProgram.phases
        ]
        return np.argmax(queueLengths)

    # use superclass's addTrainingExample() function, ie. do nothing

class CycleAgent(AgentBase):
    """Models a single traffic light. Chooses actions based on a cycle.
    Attributes
    ----------
    lightProgram: LightProgram
    numActions: dict {id: int}
    cycleCounts: list of int
    currentAction: int
    currentCount: int
    """
    def __init__(self, lightProgram, params):
        super(CycleAgent, self).__init__(lightProgram, params)
        self.cycleCounts, cycleCount = readParams(
            params,
            'CycleAgent',
            [
                ('cycleCounts', None),
                ('cycleCount', 2)
            ]
        )
        if self.cycleCounts is None:
            self.cycleCounts = [cycleCount for i in range(self.numActions)]
        self.currentAction = 0
        self.currentCount = 0
        print("Creating Cycle Agent")
    # use init function from superclass

    def chooseAction(self, features, train=True):
        if self.currentCount >= self.cycleCounts[self.currentAction]:
            self.currentAction += 1
            self.currentAction = self.currentAction % self.numActions
            self.currentCount = 0
        self.currentCount += 1
        return self.currentAction

class QAgent(AgentBase):
    """Models a single traffic light. Uses Q-learning to choose best action.

    Attributes
    ----------
    lightProgram: LightProgram
    numActions: dict {id: int}
    qFunc: QFunction
    epsilon: float 0 to 1
    batchSize: int

    batch = [] of training examples
    """
    def __init__(self, lightProgram, params):
        super(QAgent, self).__init__(lightProgram, params)
        self.epsilon, epsilonHalfLife, self.lqfProb, \
        self.batchSize, self.doTest = readParams(
            params,
            'QAgent',
            [
                ('epsilon', 1.0),
                ('epsilon_half_life', 100),
                ('lqfProb', 0.0),
                ('batch_size', 16),
                ('test', None)
            ]
        )
        self.lqfAgent = LQFAgent(lightProgram, params)
        self.qFunc = QFunction(self.numActions, lightProgram.getSchema(), params)
        self.batch = []
        if epsilonHalfLife > 0:
            self.epsilonDecayConstant = 2 ** -(1.0/epsilonHalfLife)
        else:
            self.epsilonDecayConstant = 1
        if self.doTest:
            self.epsilon = 0.0

    def chooseAction(self, features, train=True):
        """Picks an action based on a features. Set train=True to use
        epsilon-greedy algorithm for exploring actions.
        """
        if train and np.random.rand() < self.epsilon:
            if np.random.rand() < self.lqfProb:
                print("Light %s: LQF Action" % self.lightProgram.lightId)
                return self.lqfAgent.chooseAction(features)
            else:
                print("Light %s: Random Action" % self.lightProgram.lightId)
                return np.random.choice(self.numActions)
        else:
            return self.qFunc.getBestQ(features)[0]

    def addTrainingExample(self, features, action, reward, newFeatures):
        self.batch.append((features, action, reward, newFeatures))
        print("Action: %d, Reward: %f" % (action, reward))
        if self.doTest:
            return
        if len(self.batch) == self.batchSize:
            print("Training Agent %s..." % self.lightProgram.lightId)
            args = list(zip(*self.batch))
            if len(args) != 4:
                for ex in self.batch:
                    print(len(ex))
                raise Exception('Bad batch')
            self.qFunc.train(*args)
            self.batch = []
        self.epsilon *= self.epsilonDecayConstant

    def getModel(self):
        return self.qFunc.getModel()

    def setModel(self, model):
        self.qFunc.setModel(model)
