import os, sys
import json
import pickle
from simulator import Simulator
from agent import Agent

numMilliSecondsInAWeek = 1000 * 3600 * 24 * 7

# this is the main entry point of this script
if __name__ == "__main__":
    """Usage: python main.py <scenario>/<agent> [test]
        ex. python main.py std_four_way/q
    """

    # Parse Command Line
    if len(sys.argv) < 2:
        name = 'default'
    else:
        name = sys.argv[1]

    doTest = False
    if len(sys.argv) == 3 and sys.argv[2] == 'test':
        doTest = True

    # Read in params from file
    with open('../models/%s/params.json' % name) as f:
        params = json.load(f)
    params['name'] = name
    params['test'] = doTest

    # start simulation
    sim = Simulator(params)
    sim.start()
    agents = {}
    for lightId, lightProgram in sim.lightPrograms.items():
        agents[lightId] = Agent(lightProgram, params)

    # load in saved models if we are testing
    modelPath = '../models/%s/models.pkl' % name
    if doTest and os.path.exists(modelPath):
    # if os.path.exists(modelPath):
        print('Reading in learned models from file...')
        with open(modelPath, 'rb') as f:
            models = pickle.load(f)
        for lightId, model in models.items():
            agents[lightId].setModel(model)


    prevHour = None
    oldFeatures = {}
    actions = {}


    while not sim.finished():
        # for each traffic light
        # 1. get its features
        # 2. have the traffic light (usually) choose the best
        #       action based on the features, using epsilon-greedy
        # 3. apply the action
        for lightId, lightProgram in sim.lightPrograms.items():
            agent = agents[lightId]
            features = lightProgram.getFeatures()
            action = agent.chooseAction(features, train=True)
            lightProgram.applyAction(action)

            # Save features & actions for training
            oldFeatures[lightId] = features
            actions[lightId] = action

        sim.step()

        for lightId, lightProgram in sim.lightPrograms.items():
            reward = lightProgram.getReward()
            features = lightProgram.getFeatures()
            agent = agents[lightId]
            agent.addTrainingExample(
                oldFeatures[lightId],
                actions[lightId],
                reward,
                features
            )

        hour = int(sim.getCurrentTime() / 1000 / 3600)
        if hour != prevHour:
            print("Hour: %d" % hour)
        prevHour = hour

    # save model only if training
    if not doTest:
        print('Saving learned models to file...')
        models = {}
        for lightId, agent in agents.items():
            models[lightId] = agent.getModel()
        with open(modelPath, 'wb') as f:
            pickle.dump(models, f)

    sim.stop()
