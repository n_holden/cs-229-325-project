import numpy as np
from param_reader import readParams

def QFunction(numActions, schema, params):
    """Choose a QFunction class based on 'model' param."""
    model, = readParams(
        params,
        'QFunctionType',
        [('model', 'linear')]
    )
    if model == 'linear':
        return LinearQFunction(numActions, schema, params)
    elif model == 'mlp':
        return MLPQFunction(numActions, schema, params)
    else:
        raise Exception('Unknown model type: %s' % model)

class QFunctionBase(object):
    """Models a single traffic light. By default, returns random Q values.

    Attributes
    ----------
    discount: float 0 to 1
    numActions: dict {id: int}
    gamma: float 0 to 1
    schema: list of strings, the names of the parameters
    """
    def __init__(self, numActions, schema, params):
        self.gamma, = readParams(
            params,
            'QFunctionBase',
            [('gamma', 0.99)]
        )
        self.numActions = numActions
        self.schema = schema

    def getQ(self, features):
        return np.random.randn(self.numActions)

    def getBestQ(self, features):
        q = self.getQ(features)
        return np.argmax(q), np.max(q)

    def train(self, features, action, reward, newFeatures):
        pass

    def getModel(self):
        return None

    def setModel(self, model):
        pass

from sklearn.linear_model import SGDRegressor

# class SGDRegressor():
#     def __init__(self, **kwargs):
#         self.eta0 = 0.01
#         self.reg = 0.3

#     def fit(self, X, y):
#         self.coef_ = np.array([0. for _ in X[0]])
#         self.bias = 0.

#     def partial_fit(self, X, y):
#         X = np.array(X)
#         y = np.array(y)
#         print(X)
#         print(y)
#         pred = self.predict(X)
#         resid = pred - y
#         dcoef = self.eta0 * np.dot(resid, X) / len(resid)
#         dcoef += self.coef_ * self.reg
#         dbias = self.eta0 * np.mean(resid)
#         self.coef_ -= dcoef
#         self.bias -= dbias

#     def predict(self, X):
#         pred = np.dot(X, self.coef_) + self.bias
#         return pred

class LinearQFunction(QFunctionBase):
    """Models a single traffic light.

    Attributes
    ----------
    discount: float 0 to 1
    numActions: dict {id: int}
    gamma: float 0 to 1
    schema: list of strings, the names of the parameters
    models: list of SGDRegressor, one per action
    """
    def __init__(self, numActions, schema, params):
        super(LinearQFunction, self).__init__(numActions, schema, params)

        modelArgKeys = [  # Leaving out verbose, epsilon, and n_iter
            'loss', 'penalty', 'alpha', 'l1_ratio', 'fit_intercept',
            'shuffle', 'random_state', 'learning_rate', 'eta0',
            'power_t', 'warm_start', 'average'
        ]
        modelArgValues = readParams(
            params,
            'LinearQFunction',
            [(arg, None) for arg in modelArgKeys]
        )
        modelArgs = dict(
            (key, val) for key, val in zip(modelArgKeys, modelArgValues)
            if (val is not None)
        )

        # One model per action
        self.models = [SGDRegressor(**modelArgs) for i in range(numActions)]
        baseX = [[1. for _ in schema]] # inner array is just 1's for each feature
        basey = [0.]
        for model in self.models:
            model.fit(baseX, basey)

    def getQ(self, features):
        features = np.array(features)
        q = [model.predict([features]) for model in self.models]
        return q

    def train(self, features, action, reward, newFeatures):
        examples = zip(features, action, reward, newFeatures)
        target = [
            r + self.gamma * self.getBestQ(newF)[1]
            for f, a, r, newF in examples
        ]
        print target
        examples = zip(features, action, target)
        for action in range(self.numActions):
            X = [
                f
                for f, a, t in examples
                if a == action
            ]
            y = [
                t
                for f, a, t in examples
                if a == action
            ]
            if len(X) > 0:
                X = np.array(X).astype(np.float)
                y = np.array(y).astype(np.float)
                self.models[action].partial_fit(X, y)
        for action in range(self.numActions):
            weights = self.models[action].coef_

            topWeights = [(name, w)
                for _, w, name in sorted(list(zip(np.abs(weights), weights, self.schema)), reverse=True)]
            topWeights = [(name, "%0.3f" % w) for name, w in topWeights[:5]]
            print("Action %d weights: %r"  % (action, topWeights))

    def getModel(self):
        return self.models

    def setModel(self, model):
        self.models = model

class MLPQFunction(QFunctionBase):
    """Models a single traffic light.

    Attributes
    ----------
    discount: float 0 to 1
    numActions: dict {id: int}
    gamma: float 0 to 1
    schema: list of strings, the names of the parameters
    models: list of SGDRegressor, one per action
    """
    def __init__(self, numActions, schema, params):
        from sklearn.neural_network import MLPRegressor

        super(MLPQFunction, self).__init__(numActions, schema, params)

        modelArgKeys = [  # Leaving out verbose, epsilon, and n_iter
            'hidden_layer_sizes', 'activation', 'algorithm', 'alpha',
            'learning_rate', 'learning_rate_init', 'power_t',
            'shuffle', 'random_state', 'tol', 'verbose',
            'warm_start', 'momentum', 'nesterovs_momentum', 'early_stopping',
            'validation_fraction', 'beta_1', 'beta_2'
            # epsilon max_iter
        ]
        modelArgValues = readParams(
            params,
            'MLPQFunction',
            [(arg, None) for arg in modelArgKeys]
        )
        modelArgs = dict(
            (key, val) for key, val in zip(modelArgKeys, modelArgValues)
            if (val is not None)
        )

        # One model per action
        self.model = MLPRegressor(max_iter=1, **modelArgs)
        nFeatures = len(schema)
        nInit = nFeatures * 5
        baseX = np.random.rand(nInit, len(schema))
        basey = np.random.rand(nInit, self.numActions) * 0.01
        self.model.fit(baseX, basey)

    def getQ(self, features):
        return self.model.predict([features])

    def train(self, features, action, reward, newFeatures):
        target = self.model.predict(features)

        def rms(x):
            return np.sqrt(np.std(x)**2 + np.mean(x)**2)

        diff = []
        target_single = []
        for q, a, r, newF in zip(target, action, reward, newFeatures):
            orig = q[a]
            q[a] = r + self.gamma * self.getBestQ(newF)[1]
            diff.append(orig - q[a])
            target_single.append(q[a])

        target_rms = rms(target_single)
        rmse = rms(diff)
        print("Target RMS: %f RMSE: %f%%" % (target_rms, rmse / np.std(target_single)))

        weights_flat = []
        for coef in self.model.coefs_:
            weights_flat += coef.flatten().tolist()
        rms_weights = rms(weights_flat)
        print("Weight RMS: %f" % rms_weights)
        self.model.partial_fit(features, target)

    def getModel(self):
        return self.model

    def setModel(self, model):
        self.model = model
