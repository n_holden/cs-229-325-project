#!/usr/bin/env python
"""
@file    runner.py
@author  Lena Kalleske
@author  Daniel Krajzewicz
@author  Michael Behrisch
@author  Jakob Erdmann
@date    2009-03-26
@version $Id: runner.py 20433 2016-04-13 08:00:14Z behrisch $

Tutorial for traffic light control via the TraCI interface.

SUMO, Simulation of Urban MObility; see http://sumo.dlr.de/
Copyright (C) 2009-2016 DLR/TS, Germany

This file is part of SUMO.
SUMO is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.
"""
from __future__ import absolute_import
from __future__ import print_function

import os, platform
import sys
import numpy as np
import subprocess
import time

SUMO_HOME = "../sumo-0.26.0"

sys.path.append(os.path.join(os.getenv("SUMO_HOME", SUMO_HOME), "tools"))
# we need to import python modules from the $SUMO_HOME/tools directory

isWindows = (platform.system().lower() == "windows")
if isWindows:
    bin_dir = os.path.join(os.getenv("SUMO_HOME", SUMO_HOME), "bin")
    os.putenv('PATH', os.getenv('PATH') + ';' + bin_dir)

import traci

from light_program import LightProgram
from sensor import Sensor
from stats import SimulationStats
from param_reader import readParams

from socket import socket
def getfreeport(minPort=49152, maxPort=65535):
    s = socket()
    s.bind(('', 0))
    port = s.getsockname()[1]
    s.close()
    return port

class Simulator(object):
    """Simulate a traffic network.

    Attributes
    ----------
    lightPrograms: dict {id: LightProgram()}
    numLights: number of light programs
    numActions: dict {id: int}
    stats: SimulationStats
    substepLength: float, seconds
    stepLength: float, seconds
    substepsPerAction: int
    """
    def __init__(self, params):
        self.lightPrograms = {}
        self.sensors = {}
        self.numLights = None
        self.numActions = {}

        self.substepLength, self.stepLength, self.maxIter, \
            self.maxMinutes, self.withGui, scenario = readParams(
                params,
                'Simulator',
                [
                    ('substepLength', 0.5),
                    ('stepLength', 15.0),
                    ('maxIter', 1000),
                    ('maxMinutes', None),
                    ('gui', False),
                    ('scenario', 'two_by_two_3lane')
                ]
            )

        self.sumoConfigFile = '../scenarios/%(scenario)s/%(scenario)s.sumocfg' % \
            {'scenario': scenario}

        self.substepsPerAction = int(self.stepLength / self.substepLength)

        self.stats = SimulationStats(params)

        self.numIter = 0
        self.port = getfreeport()
        self.params = params

    def getCurrentTime(self):
        return traci.simulation.getCurrentTime()

    def start(self):
        """Start the simulation."""
        self._openSumo()
        traci.init(self.port)
        self._loadSensors()
        self._loadLightPrograms()
        self.wallTimeStart = time.time()

    def finished(self):
        """Check whether simulation is complete."""
        if self.numIter >= self.maxIter:
            return True
        minutes = (time.time() - self.wallTimeStart) / 60.0
        if self.maxMinutes and minutes >= self.maxMinutes:
            return True

    def _loadSensors(self):
        for sensorId in traci.inductionloop.getIDList():
            self.sensors[sensorId] = Sensor(sensorId, self.params)

    def _loadLightPrograms(self):
        for lightId in traci.trafficlights.getIDList():
            lp = LightProgram(lightId, self.params)
            lp.loadSensors(self.sensors.values())
            self.lightPrograms[lightId] = lp
            self.numActions[lightId] = lp.numActions
        self.numLights = len(self.lightPrograms)
        for lightId, lightProgram in self.lightPrograms.items():
            lightProgram.addNeighbors(self.lightPrograms.values());

    def stop(self):
        """Stop the simulation."""
        traci.close()

        if self.sumoProcess is not None:
            self.sumoProcess.wait()
            self.sumoProcess = None

    def step(self):
        """Run one iteration."""
        for i in range(self.substepsPerAction):
            self._substep()
        for sensor in self.sensors.values():
            sensor.step()
        for lp in self.lightPrograms.values():
            lp.step()
        self.stats.step()
        self.numIter += 1
        print("Iteration %d/%d" % (self.numIter, self.maxIter))

    def _substep(self):
        for sensor in self.sensors.values():
            sensor.substep()
        for lightProgram in self.lightPrograms.values():
            lightProgram.substep()
        self.stats.substep()
        traci.simulationStep()

    def _openSumo(self):
        if self.withGui:
            sumoBinary = 'sumo-gui'
        else:
            sumoBinary = 'sumo'

        if isWindows:
            sumoBinary += '.exe'

        self.sumoProcess = subprocess.Popen([
            sumoBinary,
            "-c", self.sumoConfigFile,
            "--tripinfo-output", "tripinfo.xml",
            "--step-length", str(self.substepLength),
            "--remote-port", str(self.port)
        ], stdout=sys.stdout, stderr=sys.stderr)

