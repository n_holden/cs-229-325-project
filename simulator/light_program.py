"""Defines LightProgram, LightSignal, and LightPhase."""
import traci
from collections import deque
from param_reader import readParams

class LightPhase(object):
    """Describes a set of signals to be turned on together.

    Attributes
    ----------
    program: LightProgram
    signalIndices: set of int indices that are green
    phaseString: str
    signals: list of LightSignal that are green
    incomingLanes: list of lane id (str)
    outgoingLanes: list of lane id (str)
    internalLanes: list of lane id (str)
    sensors: list of Sensor that are controlled by this phase
    """

    def __init__(self, program, phaseString):
        """Initialize the phase data."""
        self.program = program
        self.greenIndices = set(
            i for i, ch in enumerate(phaseString) if ch == 'G'
        )
        self.partialGreenIndices = set(
            i for i, ch in enumerate(phaseString) if ch == 'g'
        )
        self.signalIndices = self.greenIndices | self.partialGreenIndices
        self.phaseString = phaseString
        self.incomingLanes = []
        self.outgoingLanes = []
        self.internalLanes = []
        self.signals = []
        self.sensors = []

    def addSignal(self, signal):
        """Define which lanes this phase lets through."""
        self.signals.append(signal)
        self.incomingLanes += signal.incomingLanes
        self.outgoingLanes += signal.outgoingLanes
        self.internalLanes += signal.internalLanes

    def addSensor(self, sensor):
        """Add sensor to self.sensors."""
        self.sensors.append(sensor)

    def getYellowString(self, fromPhase):
        """Get yellow string transitioning from a phase to this one."""
        yellowIndices = (fromPhase.signalIndices - self.signalIndices)
        greenIndices = fromPhase.greenIndices & self.greenIndices
        partialGreenIndices = fromPhase.partialGreenIndices & self.partialGreenIndices
        phaseString = ''
        for i in range(self.program.numSignals):
            if i in yellowIndices:
                phaseString += 'y'
            elif i in greenIndices:
                phaseString += 'G'
            elif i in partialGreenIndices:
                phaseString += 'g'
            else:
                phaseString += 'r'
        return phaseString

    def queueLength(self, onlyStopped):
        """Get the number of cars waiting for all lanes of this phase."""
        numWaiting = 0
        for laneId in self.incomingLanes:
            vehicles = traci.lane.getLastStepVehicleIDs(laneId);
            for vehicleId in vehicles:
                if onlyStopped:
                    if traci.vehicle.getSpeed(vehicleId) < 0.1:
                        numWaiting += 1
                else:
                    numWaiting += 1
        return numWaiting


class LightSignal(object):
    """Represents a single light at an intersection.

    Attributes
    ----------
    incomingLanes: list of lane id (str)
    outgoingLanes: list of lane id (str)
    internalLanes: list of lane id (str)
    sensors: list of Sensor
    """

    def __init__(self):
        """Initialize attributes."""
        self.incomingLanes = []
        self.outgoingLanes = []
        self.internalLanes = []
        self.sensors = []

    def addLanes(self, incoming, outgoing, internal):
        """Add to incomingLanes, outgoingLanes, and internalLanes."""
        self.incomingLanes.append(incoming)
        self.outgoingLanes.append(outgoing)
        self.internalLanes.append(internal)

    def addSensor(self, sensor):
        """Add sensor to self.sensors."""
        self.sensors.append(sensor)


class LightProgram(object):
    """Represents the actions possible for a given light.

    Attributes
    ----------
    lightId: string
    currentPhase: int
    phases: list of LightPhase
    signals: list of LightSignal
    numActions: int, number of actions/phases
    numSignals: int
    neighbors: list of LightPrograms
    incomingLanes: set of laneId strings
    outogingLanes: set of laneId strings
    """

    def __init__(self, lightId, params):
        """Load and normalize the light program from SUMO."""
        self.lightId = lightId
        self.currentPhase = None
        self.phases = []
        self.signals = []
        self.neighbors = []
        self.incomingLanes = set();
        self.outgoingLanes = set();
        self.internalLanes = set();
        self._loadPhases()
        self._loadSignals()
        self.numActions = len(self.phases)
        self.numSignals = len(self.signals)
        self.historyLength, self.yellowDuration, self.rewardFunction = readParams(
            params,
            'LightProgram',
            [
                ('historyLength', 5),
                ('yellowDuration', 5),
                ('rewardFunction', 'meanInternalDistance')
            ]
        )
        self.subReward = 0.0
        self.lastReward = 0.0
        self.history = deque(maxlen=self.historyLength)

        self._saveDefinition()

    def _loadPhases(self):
        lightDefinition = traci.trafficlights.\
            getCompleteRedYellowGreenDefinition(self.lightId)
        logic = lightDefinition[0]
        for phase in logic._phases:
            phaseString = phase._phaseDef
            if 'G' in phaseString:
                self.phases.append(LightPhase(self, phaseString))
        self.phases.sort(key=lambda p: p.phaseString)

    def _loadSignals(self):
        allLinks = traci.trafficlights.getControlledLinks(self.lightId)
        self.signals = []
        for links in allLinks:
            signal = LightSignal()
            for link in links:
                signal.addLanes(*link)
            self.signals.append(signal)

        for phase in self.phases:
            for idx in phase.greenIndices:
                phase.addSignal(self.signals[idx])

        for signal in self.signals:
            self.incomingLanes |= set(signal.incomingLanes)
            self.outgoingLanes |= set(signal.outgoingLanes)
            self.internalLanes |= set(signal.internalLanes)

    def addNeighbors(self, lightPrograms):
        for lp in lightPrograms:
            if len(self.incomingLanes & lp.outgoingLanes) > 0:
                self.neighbors.append(lp)

    def loadSensors(self, sensors):
        """Add sensors to light/phases/signals based on laneId.

        Parameters
        ----------
        sensors: iterable of Sensor
        """
        foundSensors = set()
        for sensor in sensors:
            for signal in self.signals:
                if sensor.laneId in signal.incomingLanes:
                    foundSensors.add(sensor)
                    signal.addSensor(sensor)
            for phase in self.phases:
                if sensor.laneId in phase.incomingLanes:
                    foundSensors.add(sensor)
                    phase.addSensor(sensor)
        self.sensors = list(foundSensors)
        self.sensors.sort(key=lambda s: s.laneId)

    def _saveDefinition(self):
        phases = []
        for i, oldPhase in enumerate(self.phases):
            for j, newPhase in enumerate(self.phases):
                # Add yellow phase before every green phase
                yellowString = newPhase.getYellowString(oldPhase)
                duration = self.yellowDuration * 1000 # duration of yellow light
                phaseYellow = traci._trafficlights.Phase(
                    duration, duration, duration, yellowString
                )

                # Then add green phase
                duration = 100 * 1000 # stay green (for 100s) until next action
                phaseGreen = traci._trafficlights.Phase(
                    duration, duration, duration, newPhase.phaseString
                )
                phases += [phaseYellow, phaseGreen]

        lightDefinition = traci.trafficlights.\
            getCompleteRedYellowGreenDefinition(self.lightId)
        oldLogic = lightDefinition[0]

        logic = traci._trafficlights.Logic(
            oldLogic._subID,
            oldLogic._type,
            oldLogic._subParameter,
            1, # currentPhaseIndex: set to green light
            phases)
        traci.trafficlights.setCompleteRedYellowGreenDefinition(
            self.lightId, logic)

        self.currentPhase = 0

    def applyAction(self, a):
        """Switch to a given phase, including handling yellow and red lights.

        Parameters
        ----------
        a: int, 0 to numActions - 1
        """
        yellowIdx = 2 * (self.numActions * self.currentPhase + a)
        traci.trafficlights.setPhase(self.lightId, yellowIdx)
        self.currentPhase = a

    def substep(self):
        totalWaitTime = 0.0
        totalDistance = 0.0
        totalInternalDistance = 0.0
        numVehicles = 0
        numInternalVehicles = 0

        for laneId in self.internalLanes:
            vehicles = traci.lane.getLastStepVehicleIDs(laneId);
            for vehicleId in vehicles:
                totalInternalDistance += traci.vehicle.getSpeed(vehicleId)
                numInternalVehicles += 1
            # get waiting time for each lane, appent to total wait time

        for laneId in self.incomingLanes:
            vehicles = traci.lane.getLastStepVehicleIDs(laneId);
            for vehicleId in vehicles:
                if traci.vehicle.getSpeed(vehicleId) < 0.1:
                    totalWaitTime += 1.0
                totalDistance += traci.vehicle.getSpeed(vehicleId)
                numVehicles += 1
            # get waiting time for each lane, appent to total wait time

        if self.rewardFunction == 'meanDistance':
            self.subReward += totalDistance / float(numVehicles+1) # +1 to avoid div-by-0
        elif self.rewardFunction == 'totalDistance':
            self.subReward += totalDistance
        elif self.rewardFunction == 'meanInternalDistance':
            self.subReward += totalInternalDistance / float(numInternalVehicles+1)
        elif self.rewardFunction == 'totalInternalDistance':
            self.subReward += totalInternalDistance  / 50.0
        elif self.rewardFunction == 'meanWait':
            self.subReward -= totalWaitTime / float(numVehicles+1) / 10.0
        elif self.rewardFunction == 'totalWait':
            self.subReward -= totalWaitTime
        else:
            raise Exception('Unknown reward function')

    def step(self):
        """Update internal features for learning iteration."""
        self.reward = self.subReward
        self.subReward = 0.0
        self.history.append(self._getInternalFeatures())

    def _getInternalSchema(self):
        return ['%s_%s' % (self.lightId, phase.phaseString)
                for phase in self.phases]

    def _getInternalFeatures(self):
        phaseIndicator = [0.] * self.numActions
        phaseIndicator[self.currentPhase] = 1.
        return phaseIndicator

    def getSchema(self):
        """List of string keys corresponding to the list from getFeatures.

        Returns
        -------
        featureKeys: list of str
            Includes keys from sensors as well as
            Format <lightId>_<phaseString>_<timeStep>
            timeStep=0 is the most recent value
        """
        schema = []
        for sensor in self.sensors:
            schema += sensor.getSchema()
        schema += self.getActionSchema()
        for neighbor in self.neighbors:
            schema += neighbor.getActionSchema()
            for sensor in neighbor.sensors:
                schema += sensor.getSchema()
        return schema

    def getActionSchema(self):
        schema = []
        for i in range(self.historyLength):
            schema += ['%s_%d' % (key, i) for key in self._getInternalSchema()]
        return schema

    def getActionFeatures(self):
        features = []
        for i in range(self.historyLength):
            if i >= len(self.history):
                features += [0] * self.numActions
            else:
                features += self.history[-(i + 1)]
        return features

    def getFeatures(self):
        """List of numerical features for this sensor."""
        features = []
        for sensor in self.sensors:
            features += sensor.getFeatures()
        features += self.getActionFeatures()
        for neighbor in self.neighbors:
            features += neighbor.getActionFeatures()
            for sensor in neighbor.sensors:
                features += sensor.getFeatures()
        return features

    def getReward(self):
        return self.reward
